
# Html-to-m

Converts HTML into a pretty-printed Mithril.js hyperscript.

Shell:

```
cat some.html | html-to-m > some.js
```

Vim:

```
:'<,'>!html-to-m
```

# Build and install (windows)

```
yarn
yarn build-cjs
yarn build-win
cp build/html-to-m.exe c:/CmdTools
yarn clean
```

# TODO

### Parse HTML entities and output them as m.trust()

Currently all entities are decoded into plain text, so that e.g. `&nbsp;` turns into a space and gets trimmed.
See `decodeEntities` option at <https://github.com/fb55/htmlparser2/blob/f7a655b/src/Parser.ts#L120>.
Entities may appear in both text and attributes (e.g. `<div title="&lt;files&gt;">`), so `decodeEntities: false`
with a naive replacement in `format_str()` is not suitable.

# Example

```html
<div class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" title="&lt;asd&gt;">
        <p>Modal body a&nbsp;b text goes here.</p>
        <br selected contenteditable>
        <input value='500'>
        <!-- comment -->
      </div>
      <div class="modal-footer" style='qwe:asd; a:1'>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" disabled readonly>
            Save
            changes
        </button>
      </div>
    </div>
  </div>
</div>

<select class="form-select" aria-label="Default select example">
  <option selected>Open this select menu</option>
  <option value="1">One</option>
  <option value="2">Two</option>
  <option value="3">Three</option>
</select>
```

```javascript
[
    m('div.modal', {tabindex:-1}, [
        m('div.modal-dialog', [
            m('div.modal-content', [
                m('div.modal-header', [
                    m('h5.modal-title', [
                        'Modal title',
                    ]),
                    m('button.btn-close', {type:'button', 'data-bs-dismiss':'modal', 'aria-label':'Close'}),
                ]),
                m('div.modal-body', {title:'<asd>'}, [
                    m('p', [
                        'Modal body a',
                        'b text goes here.',
                    ]),
                    m('br', {selected:true, contenteditable:''}),
                    m('input', {value:500}),
                ]),
                m('div.modal-footer', {style:'qwe:asd; a:1'}, [
                    m('button.btn.btn-secondary', {type:'button', 'data-bs-dismiss':'modal'}, [
                        'Close',
                    ]),
                    m('button.btn.btn-primary', {type:'button', disabled:true, readonly:true}, [
                        'Save changes',
                    ]),
                ]),
            ]),
        ]),
    ]),
    m('select.form-select', {'aria-label':'Default select example'}, [
        m('option', {selected:true}, [
            'Open this select menu',
        ]),
        m('option', {value:1}, [
            'One',
        ]),
        m('option', {value:2}, [
            'Two',
        ]),
        m('option', {value:3}, [
            'Three',
        ]),
    ]),
]
```
