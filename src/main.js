
import {Parser} from 'htmlparser2'
import fs from 'fs'

const qw = (x) => (Array.isArray(x) ? x : (x.match(/\S+/g) ?? [ ]))
const pp = (...vs) => console.log(...vs)


const boolean_attrs = qw(`
    allowfullscreen async autofocus autoplay checked controls default defer
    disabled formnovalidate ismap itemscope loop multiple muted nomodule
    novalidate open playsinline readonly required reversed selected truespeed
`)

start().catch(error => pp('ERROR:', error))


//-----------------------------------------------------------------------------------------------------------------
// functions only below

async function start() {
    const ss = [ ]
    const tree = [{n_childs:0}]

    const parser = new Parser({
        onopentag(tag, attrs) {
            //pp('tag', tag, attrs)

            const parent = tree[tree.length - 1]
            if (parent.n_childs === 0) {
                if (tree.length > 1) {
                    ss.push(', ')
                }
                ss.push('[\n')
            }
            parent.n_childs += 1

            const selector = extract_selector(tag, attrs)
            const attrs_str = format_attrs(attrs)

            ss.push(indent(tree))
            ss.push(`m('${selector}'${format_attrs(attrs)}`) // m('div.c1.c2'

            tree.push({n_childs:0})
        },
        ontext(text) {
            text = text.replace(/^\s+|\s+$/g, '')
            if (text) {
                //pp('text', text)

                const parent = tree[tree.length - 1]
                if (parent.n_childs === 0) {
                    ss.push(', [\n')
                }
                parent.n_childs += 1

                ss.push(indent(tree))
                ss.push(format_str(text))
                ss.push(',\n')
            }
        },
        onclosetag(tag) {
            //pp('end', tag)

            const node = tree.pop()

            if (node.n_childs > 0) {
                ss.push(indent(tree))
                ss.push(`]),\n`)
            }
            else {
                ss.push(`),\n`)
            }
        },
    }, {
        //decodeEntities: false,
        lowerCaseTags: true,
        lowerCaseAttributeNames: true,
    })
    parser.write(fs.readFileSync(0, 'utf8')) // fs.promises.readFile() can't read 0
    parser.end()

    if (tree[0].n_childs <= 1) {
        ss.shift() // [\n
        ss[ss.length - 1] = ss[ss.length - 1].replace(/,\n$/, '') // ,\n
    }
    else {
        ss.push(']')
    }
    process.stdout.write(ss.join('') + '\n')
}

function indent(tree) {
    return '    '.repeat(tree.length)
}

function extract_selector(tag, attrs) {
    const ss = [tag]
    const classes = attrs.class ?? ''
    delete attrs.class
    for (const c of classes.match(/\S+/g) ?? [ ]) {
        ss.push(`.${c}`)
    }
    return ss.join('')
}

function format_attrs(attrs) {
    const ss = [ ]
    for (const name of Object.keys(attrs)) {
        if (boolean_attrs.includes(name)) {
            ss.push(`${format_key(name)}:true`)
        }
        else {
            const value = attrs[name]
            ss.push(`${format_key(name)}:${format_value(value)}`)
        }
    }
    return (ss.length ? `, {${ss.join(', ')}}` : '')
}

function format_str(s) {
    s = s.replace(/\\/g, '\\\\')
    s = s.replace(/'/g, '\\\'')
    s = s.replace(/\r?\n[ \t]*/g, ' ')
    //s = s.replace(/(&\w+;)/g, `', m.trust('$1'), '`)
    return `'${s}'`
}

function format_key(s) {
    return (/^\w[\w\d_]*$/.test(s) ? s : format_str(s))
}

function format_value(s) {
    return (/^-?\d+$/.test(s) ? s : format_str(s)) // FIXME is it safe?
}
